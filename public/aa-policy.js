class AAPolicy {
  constructor() {
    this._name = "";
    this._description = "";
    this._process = "";
    this._mode = "audit deny";
    this._fileRules = [];
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = description;
  }

  get process() {
    return this._process;
  }

  set process(process) {
    this._process = process;
  }

  get mode() {
    return this._mode;
  }

  set mode(mode) {
    this._mode = mode;
  }

  get fileRules() {
    return this._fileRules;
  }

  set fileRules(fileRules) {
    this._fileRules = fileRules;
  }

  toText() {
    text = "policy " + this._name + " " + this._process + " {<br />\n"
    this._fileRules.forEach((item) => {
      text += item.spec(this._mode)+"<br />\n";
    });
    text += "}";
    return text;
  }
}


class FileRule {
  constructor(permissions, file){
    this._permissions = permissions;
    this._file = file;
  }

  spec(mode) {
    return mode+" "+this._file+" "+this._permissions;
  }
}